#!/bin/bash
utils=($(ls -d ../../bin/*))

for i in "${utils[@]}"
do
	make clean -C $i
done
