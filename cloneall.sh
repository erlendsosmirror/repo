#!/bin/bash
utils=(cat cp date df free grep kill ls mkdir mkfifo sleep mv nano pwd rm rmdir ush sh save)

cd ../../bin

for i in "${utils[@]}"
do
	git clone git@git.erlendjs.no:erlends-os/bin/$i.git
	sleep 1
done
