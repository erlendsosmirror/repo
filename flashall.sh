#!/bin/bash
utils=($(ls -d ../../bin/*))

for i in "${utils[@]}"
do
	make flash -C $i
	sleep 1
done
