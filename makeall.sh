#!/bin/bash
utils=($(ls -d ../../bin/*))

for i in "${utils[@]}"
do
	make -C $i
done
