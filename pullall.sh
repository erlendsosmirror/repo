#!/bin/bash
utils=($(ls -d ../../bin/*) $(ls -d ../../kernel/*) $(ls -d ../../kmod/*) $(ls -d ../../lib/*) $(ls -d ../../util/*) $(ls -d ../../usr/*))

for i in "${utils[@]}"
do
	echo $i
	cd $i && git pull
done
