#!/bin/bash
utils=($(ls -d ../../bin/*) $(ls -d ../../kernel/*) $(ls -d ../../lib/*) $(ls -d ../../util/*) $(ls -d ../../usr/*))

#echo ${utils[@]}

sources=$(find ${utils[@]}/src -name '*.c')
headers=$(find ${utils[@]}/inc -name '*.h')

#echo ${sources[@]}
#echo ${headers[@]}

all=(${sources[@]} ${headers[@]})

#echo ${all[@]} | xargs wc -l

total=0

for i in "${all[@]}"
do
	num=$(cat $i | grep -vxFf exclude.txt | wc -l)
	total=$((total + num))
	echo -n $num $'\t'
	echo $i
done

echo "Total lines of code:" $total
